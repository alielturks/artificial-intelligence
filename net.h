#ifndef NET_H
#define NET_H

#include "vector"
#include "Neuron.h"
#include "cassert"
using namespace std;

typedef vector<Neuron> Layer;

class Net
{
public:
	Net();
	Net(const vector<unsigned> &topology);
	void feedForward(const vector<double> &inputVals);
	void backProp(const vector<double> &targetVals);
	void getResults(vector<double> &resultVals) const;
	~Net();
private:
	vector<Layer> m_layers;
	double m_error;
};

#endif // NET_H
