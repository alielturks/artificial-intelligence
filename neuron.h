#ifndef NEURON_H
#define NEURON_H

#include <iostream>
#include <vector>
#include <cmath>

using namespace std;
class Neuron;

typedef vector<Neuron> Layer;

//double eta = 0.2;
//double alpha = 0.5;

struct Connection {
	double weight;
	double deltaWeight;
};

using namespace std;
class Neuron
{
public:
	Neuron(unsigned numOutputs, unsigned myIndex);
	~Neuron();
	void setOutputVal(double val) { m_outputVal = val; };
	double getOutputVal(void) const { return m_outputVal; };
	void feedForward(const Layer &prevLayer);
	static double randomWeight(void) { return rand() / double(RAND_MAX); };
	void calcOutputGradients(double targetVal);
	void calcHiddenGradients(const Layer &nextLayer);
	void updateInputWeights(Layer &prevLayer);
private:
	static double transferFunction(double x);
	static double transferFunctionDerivate(double x);
	double m_outputVal;
	double sumDOW(const Layer &nextLayer) const;
	vector<Connection> m_outputWeights;
	unsigned m_myIndex;
	double m_gradient;
	static double eta;
	static double alpha;
};

#endif // NEURON_H
