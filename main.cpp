#include <iostream>
#include "Net.h"
using namespace std;

int main()
{
	vector<unsigned> topology;
	topology.push_back(2);
	topology.push_back(4);
	topology.push_back(1);
	Net myNet(topology);
	
	vector<double> inputVals, targetVals, resultVals;
	vector<double> inputVals1, inputVals2, inputVals3;
	vector<double> targetVals1, targetVals2, targetVals3;
	inputVals1.push_back(1);
	inputVals1.push_back(1);
	
	inputVals2.push_back(1);
	inputVals2.push_back(0);
	
	inputVals3.push_back(0);
	inputVals3.push_back(1);
	
	targetVals1.push_back(0);
	targetVals2.push_back(1);
	targetVals3.push_back(1);
	
	for(unsigned t = 0; t < 4000; t++ ) {
		cout << "Generation " << t << endl;
		if(t%3 == 0) {
			inputVals = inputVals1;
			targetVals = targetVals1;
		} else if(t%3 == 1) {
			inputVals = inputVals2;
			targetVals = targetVals2;
		} else if(t%3 == 2) {
			inputVals = inputVals3;
			targetVals = targetVals3;
		}
		myNet.feedForward(inputVals);
		cout << "Target value : " << targetVals[0] << endl;
		cout << "Input value  : ";
		for(unsigned i = 0; i < inputVals.size(); i++) {
			cout << " " << inputVals[i];
		}
		cout << endl;
		myNet.backProp(targetVals);
		targetVals.clear();
		myNet.getResults(resultVals);
		for(unsigned i = 0; i < resultVals.size(); i++) {
			cout << "resultVals " << resultVals[i] << endl;
		}
		cout << endl;
		
	}
	return 0;
}
